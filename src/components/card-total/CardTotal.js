import React from 'react'
import styled from 'styled-components';

export default function CardTotal(props) {
    console.log({props});
    
const StyledLeftCard =styled.div`
    display: flex;
    text-align: center;
    flex: 80%;
    flex-direction:column;
`;
const StyledRightCard =styled.div`
    flex: 25%;
`;
const StyledPrice =styled.div`
    margin-top:20px;
    font-size:20px;
    font-weight: bold;
`;
    return (
        <StyledCard colors={props.colors} style={{}}>
            <StyledLeftCard>
                <div style={{fontSize:18}}>{props.name}</div>
                <StyledPrice>${props.price}</StyledPrice>
            </StyledLeftCard>
            <StyledRightCard>
                <p style={{textAlign:'end'}}>{props.percent}%</p>
            </StyledRightCard>
        </StyledCard>
    )
    
};

const StyledCard =styled.div`
        display: flex;
        padding: 15px 25px;
        margin : 10px 5px 0 5px;
        border-radius  : 10px;
        min-width :450px;
        justify-content:space-between;
        background-image: linear-gradient(to right,#55c67e,#43a49e);

    @media (max-width: 1000px) {
        min-width : 260px;
        padding : 15px 10px;        
    }
    @media only screen and (max-width: 768px) and (min-width: 760px){
        min-height: 140px;
}
`;