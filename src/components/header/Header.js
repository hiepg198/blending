import React, { useState } from 'react'
import { NavLink } from 'react-router-dom';
import logo from '../../assets/image/logo.png';
import balance_icon from '../../assets/image/balance_icon.png'
import './header.scss'
import "antd/dist/antd.css";
import { Button, Drawer } from 'antd';
import expandIcon from '../../assets/image/icon/expand.png';
import burgerIcon from '../../assets/image/icon/burger.svg';


const link = [
    {
        name: "About bLending",
        to: "/",
    },
    {
        name: "bEarn.fi",
        href: "http://bearn.fi/",
    },
    {
        name: "bVaults",
        to: "/bVault",
    },
    {
        name: "bDollar",
        // to: "/bDollar",
        to: "/",
    },
    // {
    //     name: "Summary",
    //     to: "/Summary",
    // },
    // {
    //     name: "Staking",
    //     to: "/Staking",
    // },
];
export default function Header() {
    const MenuDestop = () => {
        return (
            <ul className="header_nav">
                {link.map((item) => (
                    <li key={item.name}>
                        {
                            item.to ? <NavLink exact activeClassName="text--menu" to={item.to}>{item.name}</NavLink> :
                                <a href={item.href} target='_blank'>
                                    {item.name}
                                </a>
                        }

                    </li>
                ))}
            </ul>
        );
    }

    const AccountButton = () => {
        return (
            <div className="account_button">
                <div className="balance">
                    <img src={balance_icon} alt="balance_icon" />
                    <p>My Balance</p>
                </div>
                <div className="button_right_header">My Balance</div>
            </div>
        )
    }

    const MobileMenu = () => {
        const [collapsed, setCollapsed] = useState(false);
        let toggleCollapsed = () => {
          setCollapsed(!collapsed);
        };
      
        return (
          <div className={`menu-wrapper ${collapsed && "menu-wrapper--active"}`}>
            <Button
              className="btn-toggle"
              // type="primary"
              onClick={toggleCollapsed}
              style={{}}
            >
              {collapsed ? <img src={expandIcon} /> : <img src={burgerIcon} />}
            </Button>
            <Drawer
              title={
              <span>
                <img src={logo} alt="logo" /> 
                <strong style={{ color:'#fff',paddingLeft:10,fontSize:24}}>bLending</strong>
              </span>
            }
              placement="left"
              closable={false}
              onClose={toggleCollapsed}
              visible={collapsed}
              className="blending-drawer"
            >
              <ul className="mobile-menu">
                {link.map((item) => (
                  <li key={item.name}>
                    { item.to ?
                      <NavLink to={item.to}>{item.name}</NavLink> :
                      <a href={item.href} target='_blank'>
                        {item.name}
                      </a>
                    }
                  </li>
                ))}
              </ul>
      
              {/* <AccountButton/> */}
            </Drawer>
          </div>
        );
      };
    return (
        <div className="bLending_header">
            <div className="logo_title">
                <a href='/'><img src={logo} alt="logo" height="42px" width="42px" /></a>
                <p style={{ color: '#fff' }}>bLending</p>
            </div>
            <MobileMenu />
            <MenuDestop />

            <AccountButton />
        </div>
    )
}
