import React from 'react'
import styled from 'styled-components'
import Header from '../header/Header'

const Page = ({ children }) => (
  <div className="blending-container">
      <Header />
      <div id="main-content">
        {children}
      </div>
      {/* <Footer /> */}
  </div>
)

const StyledPage = styled.div``

export default Page;