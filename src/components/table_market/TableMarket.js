import { Table } from 'antd';
import React, { Fragment, useState } from 'react'
import styled from 'styled-components';
import icon_market from '../../assets/image/market_logo.png'
import icon_bnb from '../../assets/image/BNB.png'
import { Modal, Button } from 'antd';
import 'antd/dist/antd.css';
import './tableMarket.scss';
import { InputNumber } from 'antd';
import { Tabs } from 'antd';
import ModalBlending from '../modal';

const data = [
    {
        key: '1',
        asset: icon_market,
        name: 'Bearn.fi',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '2',
        asset: icon_market,
        name: 'Bearn.fi',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '3',
        asset: icon_market,
        name: 'Bearn.fi',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '4',
        asset: icon_market,
        name: 'Bearn.fi',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '5',
        asset: icon_market,
        name: 'Bearn.fi',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
];
export default function TableMarket(props) {
    const [checked, setChecked] = useState(false);
    const [showPopup, setShowPopup] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { TabPane } = Tabs;
    const [message, setMessage] = useState('')

    const SwitchLabel = ({ record }) => {
        return (

            <label className="switch">
                <input type="checkbox"
                    // defaultChecked={checked}
                    checked={checked.name}
                    onChange={handleChange}
                    id={record.key}
                    name={record.name}
                    value={record.name}
                />

                <span className="slider round"></span>
            </label>
        )
    };

    const handleChange = ({ target }) => {
        console.log(target);
        // setChecked({ ...checked, [name]: !checked[name] })
    }

    const columns = [
        {
            title: 'Asset',
            dataIndex: ["asset", "name"],
            key: 'asset',
            render: (asset, info) => {
                return (
                    <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                        <img onClick={props.data ? showModal : showModalBorrow} src={info.asset} alt="market_icon" />
                        <p style={{ color: '#000', fontWeight: 600, marginLeft: 8 }}>{info.name}</p>
                    </div>
                )
            },

        },
        {
            title: 'APY',
            dataIndex: 'apy',
            key: 'apy',
            render: apy => (
                <p onClick={props.data ? showModal : showModalBorrow} style={{ color: '#68CC8D' }}>{apy} %</p>
            ),
        },
        {
            title: 'Wallet',
            dataIndex: 'wallet',
            key: 'wallet',
            render: wallet => (
                <p onClick={props.data ? showModal : showModalBorrow} style={{ color: '#676978' }}>{wallet} BFI</p>
            ),
        },
        {
            title: props.data ? 'Liquidity' : 'Collateral',
            key: 'collateral',
            dataIndex: 'collateral',
            render: (collateral, record, index) => (
                // <Switch defaultChecked={collateral} />
                props.data ? <SwitchLabel record={record} /> : <p style={{ color: '#676978' }}>${collateral}M</p>
            ),
        },
    ];

    const showModal = () => {
        setShowPopup(true)
        setIsModalVisible(true);
        let x = document.getElementsByClassName("ant-modal-header");
        if (x.length > 0) {
            x[0].style.backgroundColor = "#F5AE0A"
        }
    };

    const showModalBorrow = () => {
        setIsModalVisible(true);
    }

    const callbackModal = (childData, secondData) => {
        setIsModalVisible(childData);
    }

    return (
        <div className="table_wrapper">
            <p className="name_table">{props.nameTable}</p>

            <Table pagination={false} columns={columns} dataSource={props.source} />
            <div className="modal-wraper">

                <ModalBlending
                    tableCallback={callbackModal}
                    visible={isModalVisible}
                    showModalBlending={showPopup ? showModal : showModalBorrow}
                    supply={showPopup ? true : false}
                    table={props.data ? true : false}
                />
            </div>
        </div>

    )
}

