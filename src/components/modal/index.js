import { Table } from 'antd';
import React, { useEffect, useState } from 'react'
import styled from 'styled-components';
import icon_market from '../../assets/image/market_logo.png'
import icon_bnb from '../../assets/image/BNB.png'
import icon_wallet from '../../assets/image/wallet.png'
import icon_bnb2 from '../../assets/image/blending page supply popup.png'
import icon_bnb3 from '../../assets/image/bnb2.png'
import icon_bnb4 from '../../assets/image/bnb4.png'
import { Modal, Button } from 'antd';
import 'antd/dist/antd.css';
import { InputNumber } from 'antd';
import { Tabs } from 'antd';
import './modal.scss'

function ModalBlending({
    visible,
    tableCallback,
    supply,
    table
}) {
    const [isModalVisible, setIsModalVisible] = useState(visible);
    const { TabPane } = Tabs;

    function callback(key) {
        if (key == 2) {
            let elementRightTab = document.getElementsByClassName("ant-tabs-tab")
            // elementRightTab.style.borderRadious = '30px 0px 0px 0px !important'
            console.log(elementRightTab);
            elementRightTab[1].style.borderRadius = "30px 0px 0px 0px"
        }
    }

    const handleOk = () => {
        setIsModalVisible(false);
    };

    function onChange(value) {
        console.log('changed', value);
    }

    const sendData = () => {
        setIsModalVisible(false)
        tableCallback(false);
    }

    const renderInfoWalletAndPriceOnTop = () => (
        <>
            <div className="logo-bnb">
                <div className="box-logo-btn">
                    <div className="box-logo-top">
                        <img src={icon_bnb2}></img>
                        <span>BNB</span>
                    </div>
                    <div className="box-logo-bottom">
                        <span className="price-bnb">Price: <strong className="price-bnb-value">$428 </strong><span className="plus-percent">| + 0.7%</span></span>

                    </div>
                </div>
            </div>
            <div className="wallet-bottom">
                <div className="box-wallet">
                    <img src={icon_wallet}></img>
                    <div>
                        <span className="value-bnt">123.345.34 BNB</span>
                        <span className="value-price">$23422</span>
                    </div>
                </div>
            </div>
        </>
    )

    const renderInputNumber = () => (
        <div className="input-bnb">
            <div className="wrap-input-modal">
                <span className="min">MIN</span>
                <InputNumber size="large" width={420} min={0} defaultValue={0} onChange={onChange} />
                <span className="max">MAX</span>
            </div>
        </div>
    )

    const renderFirstTabPanel = () => (
        <TabPane tab={supply ? 'Supply' : 'Borrow'} key="1" onChange={callback}>
            <div className="tab-1">
                <div className="box-1">
                    <span className="title">{supply ? 'Supply Rates' : 'Borrow Rates'}</span>
                    <div className="box-supply-rate ">
                        <div className="inner-supply-rate inline-line">
                            <div className="left-supply-rate">
                                <img src={icon_bnb4}></img>
                                <span>Supply APY</span>
                            </div>
                            <span className="percent-supply-rate">3.67%</span>
                        </div>
                        <div className="inner-distribution inline-line">
                            <div className="left-supply-rate">
                                <img src={icon_bnb3}></img>
                                <span>Distribution APY</span>
                            </div>
                            <span className="percent-supply-rate">0.00%</span>
                        </div>
                    </div>
                </div>
                <div className="box-1">
                    <span className="title">Borrow Limit</span>
                    <div className="box-supply-rate ">
                        <div className="inner-supply-rate inline-line">
                            <div className="left-supply-rate">
                                <span>Borrow Limit</span>
                            </div>
                            <span className="percent-supply-rate">$345.3</span>
                        </div>
                        <div className="inner-distribution inline-line">
                            <div className="left-supply-rate">
                                <span>Borrow Limit Used</span>
                            </div>
                            <span className="percent-supply-rate">2.12%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="button-supply">
                <button className="modal-btn">{supply ? 'Supply' : 'Borrow'}</button>
            </div>
        </TabPane>
    )

    const renderSecondTabPanel = () => (
        <TabPane tab={supply ? 'Withdraw' : 'Repay'} key="2">
            <div className="tab-1">
                <div className="box-1">
                    <span className="title">{supply ? 'Supply Rates' : 'Borrow Rates'}</span>
                    <div className="box-supply-rate ">
                        <div className="inner-supply-rate inline-line">
                            <div className="left-supply-rate">
                                <img src={icon_bnb4}></img>
                                <span>Supply APY</span>
                            </div>
                            <span className="percent-supply-rate">3.67%</span>
                        </div>
                        <div className="inner-distribution inline-line">
                            <div className="left-supply-rate">
                                <img src={icon_bnb3}></img>
                                <span>Distribution APY</span>
                            </div>
                            <span className="percent-supply-rate">0.00%</span>
                        </div>
                    </div>
                </div>
                <div className="box-1">
                    <span className="title">Borrow Limit</span>
                    <div className="box-supply-rate ">
                        <div className="inner-supply-rate inline-line">
                            <div className="left-supply-rate">
                                <span>Borrow Limit</span>
                            </div>
                            <span className="percent-supply-rate">$345.3</span>
                        </div>
                        <div className="inner-distribution inline-line">
                            <div className="left-supply-rate">
                                <span>Borrow Limit Used</span>
                            </div>
                            <span className="percent-supply-rate">2.12%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="button-supply">
                <button className="modal-btn">{supply ? 'Withdraw' : 'Repay'}</button>
            </div>
        </TabPane>
    )

    useEffect(() => {
        let x = document.getElementsByClassName("ant-modal-header");
        console.log(table);
        console.log({ x });
        // if (x.length > 0) {
        //     supply ? x[0].style.backgroundColor = "#F5AE0A" : x[0].style.backgroundColor = "blue";
        // }
    }, [visible])


    return (
        <>
            <Modal
                centered={true}
                footer={null}
                wrapClassName={table ? 'modal-input1' : 'modal-input2'}
                width={700}
                style={{ textAlign: 'center' }}
                title={supply ? 'Supply' : 'Borrow'}
                visible={visible}
                onOk={handleOk}
                onCancel={sendData}
            >
                <div className="box-header">
                    {renderInfoWalletAndPriceOnTop()}
                </div>

                {renderInputNumber()}


                <Tabs tab="Withdraw" hideAdd={true} size={"large"} onChange={callback} type="card">
                    {renderFirstTabPanel()}
                    {renderSecondTabPanel()}
                </Tabs>
            </Modal>
        </>
    );
}

export default ModalBlending;