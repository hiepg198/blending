import logo from './logo.svg';
import './App.css';
import { Route, Switch } from 'react-router';
import HomePage from './views/home';
import styled from 'styled-components';
import { Layout } from 'antd';
import { BrowserRouter } from 'react-router-dom';
import BLending from './views/blending';
import Page from './components/newPage/Page';
import Summary from './views/blending/summary/Summary';
import Staking from './views/blending/staking/Staking';

function App() {
  return (
    <div className="">
     <BrowserRouter>
				<Layout>
					<StyleLayoutContent>
						<Switch>
							<Route path="/" exact>
								<HomePage />
							</Route>
							<Route path="/bVault" exact>
								<BLending/>
							</Route>
							<Route path="/Summary" exact>
								<Page>
									<Summary/>
								</Page>
							</Route>
							<Route path="/Staking" exact>
								<Page>
									<Staking/>
								</Page>
							</Route>
						</Switch>
					</StyleLayoutContent>
				</Layout>
			</BrowserRouter>
    </div>
  );
}
const StyleLayoutContent = styled(Layout.Content)`
  padding-left: 0px;
  padding-right: 0px;
  /* background : #000; */
`;
export default App;
