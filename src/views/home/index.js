import React from 'react'
import Page from '../../components/newPage/Page'
import HomeContent from './HomeContent';
import '../../components/header/header.scss'

export default function HomePage() {
    return (
        <Page>
            <HomeContent/>
        </Page>
    )
}
