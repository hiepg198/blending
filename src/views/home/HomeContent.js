import { Col, Row } from 'antd'
import React from 'react'
import styled from 'styled-components';
import TableMarket from '../../components/table_market/TableMarket';
import './HomeContent.scss'
import icon_market from '../../assets/image/market_logo.png'

const dataDemo = [
    {
        key: '1',
        asset: icon_market,
        name: 'Bearn.fi1',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '2',
        asset: icon_market,
        name: 'Bearn.fi2',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '3',
        asset: icon_market,
        name: 'Bearn.fi3',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '4',
        asset: icon_market,
        name: 'Bearn.fi',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
    {
        key: '5',
        asset: icon_market,
        name: 'Bearn.fi',
        apy: 0.72,
        wallet: 3846,
        collateral: 189.5,
    },
];

export default function HomeContent() {

    return (
        <StyledWrapper>
            <div className="home-content">
                <div className="supply-balance common-colum">
                    <span className="title">Supply Balance</span>
                    <span className="price">$396,689.58</span>
                </div>

                <div className="net-apy-percent common-colum">
                    <span className="title">NET APY</span>
                    <span className="percent">186%</span>
                </div>

                <div className="borrow-balance common-colum">
                    <span className="title">Borrow Balance</span>
                    <span className="price">$15,000.00</span>
                </div>

            </div>
            <div className="borrow-limit">
                <p className="borrow-limit-title">Borrow Limit</p>
                <div className="box-percent-limmit">
                    <span className="borrow-percent">25%</span>
                    <ul className="box-item">
                        <li><span className="active"></span></li>
                        <li><span className="active"></span></li>
                        <li><span className="active"></span></li>
                        <li><span className="active"></span></li>
                        <li><span className="active"></span></li>
                        <li><span className="active"></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                    </ul>
                    <span className="borrow-price">$223.442</span>
                </div>
            </div>
            <StyledTableWrapper>
                <TableMarket nameTable="Supply Markets" data="test table" source={dataDemo} />
                <TableMarket nameTable="Borrow Markets" source={dataDemo} />
            </StyledTableWrapper>
        </StyledWrapper>
    )
}
const StyledWrapper = styled.div`
    display: flex;
    flex-direction: column;
    /* align-items: center; */
    width: 100%;
    padding : 15px;
    @media (max-width: 780px) {
        padding : 15px 0;
    }
`;
const StyledTableWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    @media (max-width: 780px) {
        flex-direction: column;
    }
`;
