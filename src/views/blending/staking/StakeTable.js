import { Input } from 'antd';
import React from 'react';
import icon_bnb from '../../../assets/image/BNB.png';
import './staking.scss';

export default function StakeTable(props) {

    const StakeAndUnstake =(props)=>{
        return (
            <div className="trade-stake">
                <p style={{fontWeight:'bold'}}>I want to stake</p>
                <p>Available ibBNB Balance : 0.00</p>
                {/* <div className="input-stake"> */}
                    <Input className="input-stake" 
                    addonAfter={<p className="button-setmax">Max</p>} 
                    defaultValue="0.00" />
                {/* </div> */}
                <div className="button-stake">{props.action ??'Stake'}</div>
            </div>
        )
    }

    return (
        <div className="stake-table-container">
            <div className="row-1">
                <div className="logo-and-name">
                    <img src={icon_bnb} alt="icon_bnb"/>
                    <p>bBNB</p>
                </div>
                <div className="apy-and-apr">
                    <div className="apy-percent apy">APY 41%</div>
                    <div className="apy-percent apr">APY 41%</div>
                </div>
                <div className="bfi-and-bdo">
                    <span>
                        <p>BFI earned : 100</p>
                        <p>BDO earned : 100</p>
                    </span>
                </div>
            </div>
            
            <div className="row-2">
                <div className="stake">
                    <StakeAndUnstake/>
                </div>
                <div className="stake">
                    <StakeAndUnstake action="Unstake"/>
                </div>
                <div className="pending-reward">
                    <p>Pending Reward</p>
                    <strong>0</strong>
                    <div className="button-claim">Claim</div>

                </div>
            </div>
        </div>
    )
}
