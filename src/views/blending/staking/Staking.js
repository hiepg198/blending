import { Col, Row } from 'antd';
import React from 'react';
import styled from 'styled-components';
import CardTotal from '../../../components/card-total/CardTotal';
import StakeTable from './StakeTable';
import './staking.scss';

export default function Staking() {

    const CardPending = (props) => {
        return (
            <StyledCard>
                <StyledTextReward>
                    <span>Pending Rewards: </span>
                    <span>{props.reward ?? '$10,000,000.00'}</span>
                </StyledTextReward>
                <StyledButtonClaim>Claim All</StyledButtonClaim>
            </StyledCard>
        )
    }
    return (
        <div className="staking_container">
            <Row justify="center" align="middle">
                <Col className="card-total-supply" xs={24} sm={24} md={12}>
                    <CardTotal name='Total Supply' price='12,345,678.90' percent='+ 420' />
                </Col>
                <Col xs={24} sm={24} md={12}>
                    <CardPending />
                </Col>
            </Row>
            <StakeTable/>
            <StakeTable actions={''}/>
        </div>
    )
}
const StyledCard = styled.div`
    display: flex;
    padding: 15px 25px;
    margin : 10px 5px 0 5px;
    border-radius  : 10px;
    min-width :450px;
    flex-direction : column;
    align-items : center;
    text-align : center;
    background-image: linear-gradient(to right,#9cbb4a,#c3b52e);
    @media (max-width: 1000px) {
        min-width : 260px;
        padding : 15px 10px;        
    }
`;
const StyledTextReward = styled.span`
    font-size: 18px;
`;
const StyledButtonClaim = styled.button`
    padding: 5px 15px;
    margin : 20px 0 0;
    background-color: unset;
    border-radius: 5px;
    border: 1px solid #fff;
    &:hover{
        border: 1px solid yellow;
        color: yellow;
    }
`;