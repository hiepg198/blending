import React, { useState } from 'react'
import styled from 'styled-components';
import Page from '../../components/newPage/Page'
import HomeContent from '../home/HomeContent';
import Staking from './staking/Staking';
import Summary from './summary/Summary';

export default function BLending() {
    const listTab = ['Summary', 'Lending', 'Staking']
    const [tab, setTab] = useState('Summary');

    return (
        <Page>
            <StyledCardWrapper>
                <StyledCardTab>
                    {listTab.map((item, index) => (
                        <StyledTab key={item}
                            style={{
                                backgroundColor: tab === item ? '#F5AE0A' : '#2c2f4f',
                                color: tab === item ? '#000' : '#fff',
                                borderTopLeftRadius: index === 0 && 8,
                                borderBottomLeftRadius: index === 0 && 8,
                                borderTopRightRadius: index === 2 && 8,
                                borderBottomRightRadius: index === 2 && 8,
                            }}
                            onClick={() => setTab(item)}>{item}
                        </StyledTab>
                    )
                    )}
                </StyledCardTab>

                {tab === 'Summary' && <Summary />}
                {tab === 'Staking' && <Staking />}
                {tab === 'Lending' && <HomeContent />}
            </StyledCardWrapper>
        </Page>
    )
}

const StyledCardWrapper = styled.div`
    max-width: 1256px;
    margin: 0 auto;
    display: flex;
    align-items: center;
    flex-direction:column;
`;

const StyledCardTab = styled.div`
    display: flex;
    flex-direction: row;
    text-align: center;
    /* background-color: #2c2f4f; */
`;
const StyledTab = styled.div`
    width:100px;
    padding : 5px 0;
`;
