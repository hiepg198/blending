import { Col, Row } from 'antd'
import React, { useState } from 'react';
import CardTotal from '../../../components/card-total/CardTotal';
import './summary.scss';
import logo_market from '../../../assets/image/market_logo.png';
import arrow_more from '../../../assets/image/arrow_more.png';
import styled from 'styled-components';


const data = {
    image: '',
    market: 'bEarn Finance',
    total_supply: 39.26,
    supply_apy: 2.45,
    total_borrow: 11.33,
    borrow_apy: 9.70,
    children: [{
        name: 'Price', value: '897'
    }]
};
const count = [0, 1, 2, 3, 4];

export default function Summary() {
    const [rowSelected, setRowSelected] = useState(null);
    const colors = ['#9cbb4a', '#c3b52e'];

    return (
        <div className='summary-container'>
            <StyledHeaderTitle>
                <StyledContentHeader>
                    <p style={{ color: '#F5AE0A' }}>Earn rewards</p>
                    <p style={{ color: '#7F8197' }}> by lending your idle assets</p>
                </StyledContentHeader>
            </StyledHeaderTitle>
            <Row justify="center" align="middle">
                <Col xs={24} sm={24} md={12}>
                    <CardTotal name='Total Supply' price='12,345,678.90' percent='+ 420' />
                </Col>
                <Col xs={24} sm={24} md={12}>
                    <CardTotal colors={colors} name='Total Borrow' price='2,345,678.90' percent='- 0.48' />
                </Col>
            </Row>
            {/* <div className='list-market'> */}
            <div className="table-market">
                <div className="header-table">
                    <p className="col-1">Market</p>
                    <p className="col-2">Total Supply</p>
                    <p className="col-3">Supply APY</p>
                    <p className="col-4">Total Borrow</p>
                    <p className="col-5">Borrow APY</p>
                </div>

                {count.map((item, index) => (
                    <div className="body-table" key={index}>
                        <div onClick={() => {
                            if (rowSelected === index) setRowSelected(null)
                            else setRowSelected(index);
                        }}
                            className="content-table"
                            style={{ backgroundColor: rowSelected === index ? '#f4f5f9' : '#fff' }}>
                            <div className={rowSelected === index ?"content-row border-row-active":"content-row"}
                            // style={{borderLeftColor:'#55c67e',borderLeftWidth: 3}}
                            >
                                <div className="col-1">
                                    <img src={logo_market} alt="logo-market" height={40} />
                                    <div style={{ marginLeft: 5 }}>
                                        <p>{data.market}</p>
                                        <p style={{ color: '#979797' }}>BFI</p>
                                    </div>
                                </div>
                                <p className="col-2">${data.total_supply}M</p>
                                <p className="col-3 supply-apy">{data.supply_apy}%</p>
                                <p className="col-4">%{data.total_borrow}M</p>
                                <p className="col-5">{data.borrow_apy}%</p>
                            </div>
                            {rowSelected === index &&
                                <div className="detail-market">
                                    <div className="header-detail-market">
                                        <p>Market Details</p>
                                        <img className="img-arrow" src={arrow_more} alt="details" height="8" />
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                    <div className="header-table item-detail-market">
                                        <p>Name</p>
                                        <p>$78599</p>
                                    </div>
                                </div>}
                        </div>

                    </div>
                ))}

            </div>
            {/* </div> */}
        </div>
    )
}
const StyledHeaderTitle = styled.div`
    text-align: center;
`;
const TitlePage = styled.div`
    /* text-align: center; */
    font-weight: bold;
`;
const StyledContentHeader = styled.span`
    padding:10px 0;
    p{
        font-size: 20px;
        font-weight: bold;
    }
`;