import moment from "moment";
import React, { useState, useEffect } from "react";
import './Cout.css';


export default function Countdown(props) {
    const [days, setDates] = useState(undefined);
    const [hours, setHours] = useState(undefined);
    const [minutes, setMinutes] = useState(undefined);
    const [seconds, setSecounds] = useState(undefined);

    function polarToCartesian(
        centerX,
        centerY,
        radius,
        angleInDegrees
    ) {
        console.log(angleInDegrees);
        var angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;
        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }

    function describeArc(
        x,
        y,
        radius,
        startAngle,
        endAngle
    ) {
        // console.log( x,
        //     y,
        //     radius,
        //     startAngle,
        //     endAngle);
        var start = polarToCartesian(x, y, radius, endAngle);
        var end = polarToCartesian(x, y, radius, startAngle);
        // console.log({start});

        var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [
            "M",
            start.x,
            start.y,
            "A",
            radius,
            radius,
            0,
            largeArcFlag,
            0,
            end.x,
            end.y
        ].join(" ");
        console.log({d});
        return d;
    }
    const SVGCircle = ({radius}) => (
        <svg className="countdown-svg">
            <path
                fill="none"
                stroke="#333"
                strokeWidth="2"
                d={describeArc(50, 50, 48, 0, radius)}
            />
        </svg>
    );

    function mapNumber(
        number,
        in_min,
        in_max,
        out_min,
        out_max
    ) {
        return  (number - in_min) * (out_max - out_min) / (in_max - in_min) + out_min ;
    }

    useEffect(() => {
        setInterval(() => {
            const { timeTillDate, timeFormat } = props;
            console.log({timeTillDate});
            const then = moment(timeTillDate, timeFormat);
            let now = moment();
            let countdown = moment(then - now);
            console.log({then},{now},{countdown});
            setDates(countdown.format("D"));
            setHours(countdown.format("HH"));
            setMinutes(countdown.format("mm"));
            setSecounds(countdown.format("ss"));
        }, 1000);
    }, []);

    if(!seconds){
        return null;
    }
    return (
            <div>
                <h1>Countdown</h1>
                <div className="countdown-wrapper">
                    {days && (
                        <div className="countdown-item">
                            <SVGCircle radius={mapNumber(days, 30, 0, 0, 360)} />
                            {days}
                            <span>days</span>
                        </div>
                    )}
                    {hours && (
                        <div className="countdown-item">
                            <SVGCircle radius={mapNumber(hours, 24, 0, 0, 360)} />
                            {hours}
                            <span>hours</span>
                        </div>
                    )}
                    {minutes && (
                        <div className="countdown-item">
                            <SVGCircle radius={mapNumber(minutes, 60, 0, 0, 360)} />
                            {minutes}
                            <span>minutes</span>
                        </div>
                    )}
                    {seconds && (
                        <div className="countdown-item">
                            <SVGCircle radius={mapNumber(seconds, 60, 0, 0, 360)} />
                            {seconds}
                            <span>seconds</span>
                        </div>
                    )}
                </div>
            </div>
    );
}